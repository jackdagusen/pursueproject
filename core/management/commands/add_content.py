from django.core.management.base import BaseCommand, CommandError

from likert.models import Question, Assessment
from register.models import User
from quiz.models import Category, Quiz
from multichoice.models import MCQuestion, Answer

class Command(BaseCommand):
    help = 'Creates the dataset for skill assessment and exams.'    

    def handle(self, *args, **options):
        def assessment_skills():
            skills = [
                'Programming Skills',
                'Interpersonal Skills',
                'Knowledge in machines and fabrication',
                'Creativity',
                'Ethics',
                'Interest in Technology',
                'Visualization Skills',
                'Communication Skills',
                'Curiosity',
                'Mathematical Skills',
                'Attention to Detail',
                'Aesthetic Skills',
                'Visualization of 3D objects',
                'Drafting Skills',
                'Business-Mindedness',
                'Analytical Skills',
                'Intuitiveness',
                'Organizational Skills',
                'Open-Mindedness',
                'Critical Thinking',
                'Resourcefulness',
                'Passion',
                'Time Management',
                'Teamwork',
                'Perseverance',
                'Patience',
                'Flexibility and Versatility',
                'Mental/Emotional Stability',
                'Being Advanced',
                'Determination',
            ]

            descriptions = [
                'The art of using various coding languages to write computer commands and instructions for computers, applications, or software programs about the actions it must perform and how it is performed.',
                'Traits relied on upon interacting and communicating with others.',
                'The act of designing, using and repairing tools and machines including computers',
                'The act of creating things based on one’s imagination, the ability to develop traditional ways of thinking and coming up with original and better ideas.',
                'The act of making decisions based on moral principles.',
                'One whose fascinated with technological advancements.',
                'The act of envisioning and imagining things with good perception.',
                'The ability to accept and provide information verbally and non-verbally.',
                'The desire to know and understand things.',
                'The ability to perform mathematical calculations.',
                'The ability to complete tasks with concern for all areas involved, even the small ones.',
                'The ability to make things beautiful and pleasing to the eye.',
                'The act of projecting 3D objects in one’s mind easily.',
                'The ability to express one’s thoughts by means of writing and/or drawing.',
                'The ability of being able to assess the needs of people and capitalizing on those to make profit.',
                'The ability to identify, define and solve problems.',
                'The ability to discern the true nature of a situation without any proof or evidence.',
                'The ability to achieve tasks efficiently and in an organized manner.',
                'The ability to accept and consider ideas from others that are new.',
                'The ability to analyze and evaluate different situations clearly and with logical reasoning.',
                'Being able to use resources to the fullest extent.',
                'The act of excitement and enthusiasm in one’s work.',
                'The ability to manage and control time to perform tasks productively and efficiently.',
                'The ability to work with a team in order to achieve tasks	.',
                'The ability to continue tasks despite of its difficulty.',
                'The ability to wait in a reasonable amount of time.',
                'The ability to perform multiple tasks simultaneously at a high level and not to be worried upon given a challenging situation.',
                'The ability to remain calm, balanced and stable.',
                'The act of being ahead on things.',
                'The act of refusing to be prevented from completion of tasks.',
            ]

            Assessment.objects.create(title='Assessment Test')

            for skill, description in zip(skills, descriptions):
                Question.objects.create(question=skill, description=description, assessment_id=1)

            print(f'assessment done')

        def exam_content():
            ##CATEGORY GENERATOR##
            categories = [
                'GE',
                'CE',
                'ECE',
                'EE',
                'ME'
                ]
            
            Category.objects.bulk_create([Category(category=category) for category in categories])


            ##QUIZ GENERATOR##
            quizzes = [
                'General Engineering',
                'Civil Engineering',
                'Electronics Engineering',
                'Electrical Engineering',
                'Mechanical Engineering',
                ]

            urls = [
                'ge',
                'ce',
                'ece',
                'ee',
                'me'
                ]   

            category = Category.objects.all()

            max_questions = [20,10,10,10,10]

            answers_at_end = [True,True,True,True,True]

            exam_paper = [True,True,True,True,True]

            single_attempt = [True,True,True,True,True]

            pass_mark = [0,0,0,0,0]


            Quiz.objects.bulk_create([Quiz(
                title=quizzes[i], 
                url=urls[i], 
                category=category[i], 
                max_questions=max_questions[i],
                answers_at_end=answers_at_end[i],
                exam_paper=exam_paper[i],
                single_attempt=single_attempt[i],
                pass_mark=pass_mark[i]) for i in range(0,5)
            ])

            ge_questions = [
                'In which quadrant is the point (-15, -19)?',
                'It is the rate of change of function with respect to a variable.',
                'What do you call the occurrence of differentiating the position twice?',
                'These are the branches of mathematics that are concerned with the determination, properties, and application of derivatives and differentials.',
                'Product of Force and Velocity is called ______.',
                'If pressure increases, the boiling point  _____ of any substance.',
                'Which law is also called the law of inertia?',
                'When a body is in motion, what energy does it possess?',
                'When a body is at rest, what energy does it possess?',
                'What is the approximate velocity of light in m/s?',
                'This is a value in statistics that specifies the average of a given set of numbers.',
                'Upon flipping 3 coins, how many possible outcomes can be obtained?',
                'What value will be obtained if a mass of an object is multiplied by its acceleration?',
                "These are values that don't have direction, it simply is a magnitude. ",
                'It is a vector quantity that depicts how far an object is from its previous position. ',
                'What is the value of 0 divided by 1?',
                'All ____ are relations but not all relations are ____',
                'Electrons ____ electrons and _____ Protons. ',
                'It is a standard unit of measurement for small particles.',
                'These are elements that have properties of metals and nonmetals which are also known as semiconductors due to their capability of conducting electricity in a certain condition.',
            ]

            ce_questions = [
                'A construction block has a length of 4cm and an area of 12cm2, what is the value of its height?',
                'What do you call the summation of forces on a rigid body?',
                'The force that is perpendicular to the surface is called?',
                'The ___ is a type of support in structures that allows rotation but restricts horizontal and vertical movement.',
                'This type of support in structures which restricts rotation, horizontal and vertical movement.',
                'If twenty men can do the job in 50 days, how many days will it take 100 men to do the job?',
                'In terms of Analytical Geometry, a  ___ can be formed with at least two points.',
                'Given the 3D figure below, which of the following would be seen if viewed from the FRONT?',
                'What is the magnitude and direction of the acceleration of a free falling object?',
                '____ is used to visualize a rigid body together with the forces acting upon it.',
            ]

            ece_questions = [
                'An object typically used for simulating circuitry.',
                'In Ohm’s Law the Voltage is __________ to the current in the circuit.',
                'A cable wire measuring 615 cm long is cut into two parts such that the longer part is four times as long as the shorter part. What are the lengths, in cm, of the two parts?',
                'An unwanted signal which interferes with the original message.',
                'What connector is commonly used for Ethernet Cables?',
                'What type of topology uses a hub as the backbone for the entire network?',
                'AC Voltage resembles what type of wave?',
                'This device allows DC to flow through it but prevents AC from flowing through it.',
                'This device stores electrical charges in it which makes it somewhat of a voltage source.',
                '_____ is the number of times a timely event has occurred in a particular time period. Its unit is called Hertz or cycles per second.',
            ]

            ee_questions = [
                'Preferable connection for light bulbs.',
                'Power is ________ to both the voltage and current.',
                'A parameter that remains constant across all circuit elements in a series circuit.',
                'A parameter that remains constant across all circuit elements in a parallel circuit.',
                'The voltage measured across a series short is ________.',
                'The parameter that is being measured to calculate the electric bill is ____.',
                'What is the voltage of the outlet in the Philippines?',
                'A device that supplies Electromotive Force',
                'Find the current through a resistor of resistance 5 Ω if the voltage across the resistor is 8 V.',
                'This law states that the summation of all the potential differences of the components within a closed loop of a circuit is equal to 0.',
            ]

            me_questions = [
                'It is a set of toothed wheels that is combined altogether to drive a mechanism.',
                'It is a process that concerns the exchange and generation of thermal energy to different physical systems.',
                'It is the oscillatory motion created by a mechanism or a machine.',
                'What is the standard room temperature in Celsius?',
                'Joule-Thomson coefficient is _______ for an expansion process that results in cooling.',
                'Which gas contributes maximum to the heating value of natural gas?',
                'Given the 3D figure below, which of the following would be seen if viewed from the TOP?',
                'Steam has a freezing point of _______.',
                'It is a type of vibrations that are also known as Transient Vibrations.',
                'Deterministic vibrations are ____________.',
            ]

            courses_questions = [ge_questions, ce_questions, ece_questions, ee_questions, me_questions]
            # quiz_query = Quiz.objects.all()

            def create_questions(course_category, questions):
                for question in questions:
                    MCQuestion.objects.create(
                        answer_order='random',
                        category= course_category,
                        content= question,
                        ) 
                print(f'{course_category} questions done')

            for i in range(len(courses_questions)):
                create_questions(course_category=category[i], questions=courses_questions[i])
            
            ge_questions = MCQuestion.objects.filter(category__category="GE")
            ce_questions = MCQuestion.objects.filter(category__category="CE")
            ece_questions = MCQuestion.objects.filter(category__category="ECE")
            ee_questions = MCQuestion.objects.filter(category__category="EE")
            me_questions = MCQuestion.objects.filter(category__category="ME")
            
            through = MCQuestion.quiz.through

            def get_last_id(queryset):
                get_id = queryset.order_by().last().id
                return get_id

            for i in range(1, len(MCQuestion.objects.all())+1):
                if 0 <= i <= get_last_id(ge_questions):
                    id = 1
                if get_last_id(ge_questions) < i <= get_last_id(ce_questions):
                    id = 2
                if get_last_id(ce_questions) < i <= get_last_id(ece_questions):
                    id = 3
                if get_last_id(ece_questions) < i <= get_last_id(ee_questions):
                    id = 4
                if get_last_id(ee_questions) < i <= get_last_id(me_questions):
                    id = 5

                through.objects.bulk_create([through(quiz_id=id,question_id=i)])


            ge_choices_1 = [
            'quadrant 3',
            'quadrant 1',
            'quadrant 2',
            'quadrant 4',
            ]

            ge_choices_2 = [
            'derivative',
            'integral',
            'function',
            'laplace transform',
            ]

            ge_choices_3 = [ 
            'acceleration',
            'distance',
            'velocity',
            'jerk',
            ]

            ge_choices_4 = [ 
            'Integral Calculus and Differential Calculus',
            'Trigonometry',
            'Algebra',
            'Geometry',
            ]

            ge_choices_5= [
            'Power',
            'Work',
            'Energy',
            'Distance',
            ]

            ge_choices_6 = [
            'increases',
            'decreases',
            'doesn’t change',
            'none of the above'
            ]

            ge_choices_7 = [
            'Newton’s First Law',
            'Newton’s Second Law',
            'Newton’s Third Law',
            'Newton’s Fourth Law',
            ]

            ge_choices_8 = [ 
            'Kinetic Energy',
            'Static Energy',
            'Dynamic Energy',
            'Potential Energy',
            ]

            ge_choices_9 = [ 
            'Potential Energy',
            'Static Energy',
            'Dynamic Energy',
            'Kinetic Energy',
            ]

            ge_choices_10 = [ 
            '300000000 m/s',
            '100000000 m/s',
            '200000000 m/s',
            '400000000 m/s',
            ]

            ge_choices_11 = [ 
            'mean',
            'variance',
            'standard deviation',
            'normal distribution',
            ]

            ge_choices_12 = [ 
            '8',
            '6',
            '12',
            '16',
            ]

            ge_choices_13 = [ 
            'Force',
            'Work',
            'Power',
            'Energy',
            ]

            ge_choices_14 = [ 
            'Scalar Quantity',
            'Vector Quantity',
            'Force Quantity',
            'Distance',
            ]

            ge_choices_15 = [ 
            'Displacement',
            'Distance ',
            'Velocity',
            'Acceleration',
            ]

            ge_choices_16 = [ 
            '0',
            'undefined',
            'indeterminate',
            'infinity',
            ]

            ge_choices_17 = [ 
            'Functions',
            'Quantities ',
            'Values',
            'All of the above',
            ]

            ge_choices_18 = [ 
            'Repel; Attract',
            'Attract; Repel',
            'Repel; Repel',
            'Attract; Attract',
            ]

            ge_choices_19 = [ 
            'Moles',
            'Grams',
            'Liters',
            'Pascals',
            ]

            ge_choices_20 = [
            'Metalloids',
            'Gases',
            'Sedimentary Rocks',
            'Insulators	',
            ]

            ce_choices_1 = [
            '3 cm',
            '4 cm',
            '5 cm',
            '6 cm',	
            ]

            ce_choices_2 = [
            'Resultant Force',
            'Internal Force',
            'Normal Force',
            'External Force',	
            ]

            ce_choices_3 = [
            'Normal Force',
            'Internal Force',
            'Resultant Force',
            'External Force',
            ]

            ce_choices_4 = [
            'Pin Support',
            'Fixed Support',
            'Roller Support',
            'Straight Support',
            ]

            ce_choices_5 = [
            'Fixed Support',
            'Pin Support',
            'Roller Support',
            'Straight Support',
            ]

            ce_choices_6 = [  
            '10',
            '15',
            '30', 
            '20', 
            ]

            ce_choices_7 = [
            'line',
            'arc',
            'angle',
            'plane',

            ]

            ce_choices_8 = [
            'A',
            'B',
            'C',
            'D',
            ]

            ce_choices_9 = [
            '– 9.81 m/s2',
            '– 9.81 km/s2',
            '9.81 m/s2',
            '9.81 km/s2',
            ]

            ce_choices_10 = [
            'Free Body Diagram',
            'Sketch',
            'Design',
            'Force Diagram',
            ]

            ee_choices_1 = [
            'Parallel',
            'Series',
            'Delta',
            'Wye',
            ]

            ee_choices_2 = [
            'Directly Proportional',
            'equal',
            'Inversely Proportional',
            'not related',
            ]

            ee_choices_3 = [
            'Current',
            'Voltage',
            'Power',
            'Energy',
            ]


            ee_choices_4 = [
            'Voltage',
            'Current',
            'Power',
            'Electrical Energy',
            ]

            ee_choices_5 = [
            '0',
            'undefined',
            'indeterminate',
            'infinity',
            ]

            ee_choices_6 = [
            'Electrical Energy',
            'Voltage',
            'Current',
            'Power',
            ]

            ee_choices_7 = [
            '220 V AC RMS',
            '120 V AC RMS',
            '240 V AC RMS',
            '330 V AC RMS',
            ]

            ee_choices_8 = [
            'Battery',
            'Inductor',
            'Transmission Line',
            'Transformer',
            ]

            ee_choices_9 = [
            '1.6 A ',
            '13 A',
            '2 A',
            '40 A',
            ]

            ee_choices_10 = [
            'Kirchhoff’s Voltage Law',
            'Kirchhoff’s Current Law',
            'Faraday’s Law',
            'Coulomb’s Law', 
            ]  

            ece_choices_1 = [
            'breadboard',
            'ohmmeter',
            'PCB',
            'microchip',
            ]

            ece_choices_2 = [
            'Directly Proportional',
            'equal',
            'Inversely Proportional',
            'not related',
            ]

            ece_choices_3 = [
            '123 cm and 492 cm',
            '124 cm and 491 cm',
            '125 cm and 490 cm',
            '126 cm and 489 cm',
            ]

            ece_choices_4 = [
            'Noise',
            'Interference',
            'Attenuation',
            'Distortion',
            ]

            ece_choices_5 = [
            'RJ45',
            'USB 2.0',
            'USB 3.0',
            'CAT 5E',
            ]

            ece_choices_6 = [
            'Star Topology',
            'Bus Topology',
            'Mesh Topology',
            'Ring Topology',
            ]

            ece_choices_7 = [
            'Sinusoidal Wave',
            'Triangular Wave',
            'Rectangular Wave',
            'Square Wave',
            ]

            ece_choices_8 = [
            'Capacitor',
            'Resistor',
            'Inductor',
            'Diode',
            ]

            ece_choices_9 = [
            'Capacitor',
            'Resistor',
            'Inductor',
            'Diode',
            ]

            ece_choices_10 = [
            'Frequency',
            'Hertical',
            'Period',
            'Duty Cycle',
            ]

            me_choices_1 = [
            'Gears',
            'Hinge',
            'Spring',
            'Saw',
            ]

            me_choices_2 = [
            'Heat Transfer',
            'Conduction',
            'Boiling',
            'Adaptation',
            ]

            me_choices_3 = [
            'Mechanical vibration',
            'Mechanical disturbance',
            'Mechanical occurrence',
            'Mechanical transmission',
            ]

            me_choices_4 = [
            '20-22 °C',
            '26-28 °C',
            '15-19 °C',
            '10-15 °C',
            ]

            me_choices_5 = [
            'Positive',
            'Negative',
            'indeterminate',
            'undefined',
            ]

            me_choices_6 = [
            'CH4',
            'CO',
            'CO2',
            'H2',
            ]

            me_choices_7 = [
            'C',
            'B',
            'A',
            'D',
            ]

            me_choices_8 = [
            '0°C',
            '-15.6°C',
            '-99.2°C',
            ' 5°C',
            ]

            me_choices_9 = [
            'Damped Vibrations',
            'Undamped Vibrations',
            'Torsional Vibrations',
            'Transverse Vibrations',
            ]

            me_choices_10 = [
            'Vibrations caused due to known exciting force.',
            'Vibrations caused due to unknown exciting force.',
            'Vibrations which are aperiodic in nature.',
            'Vibrations which are periodic in nature. ',
            ]



            ge_choices = [ge_choices_1, ge_choices_2, ge_choices_3, ge_choices_4, ge_choices_5, ge_choices_6, ge_choices_7, ge_choices_8, ge_choices_9, ge_choices_10,
                        ge_choices_11, ge_choices_12, ge_choices_13, ge_choices_14, ge_choices_15, ge_choices_16, ge_choices_17, ge_choices_18, ge_choices_19, ge_choices_20,]
            ce_choices = [ce_choices_1, ce_choices_2, ce_choices_3, ce_choices_4, ce_choices_5, ce_choices_6, ce_choices_7, ce_choices_8, ce_choices_9, ce_choices_10,]
            ece_choices = [ece_choices_1, ece_choices_2, ece_choices_3, ece_choices_4, ece_choices_5, ece_choices_6, ece_choices_7, ece_choices_8, ece_choices_9, ece_choices_10,]
            ee_choices = [ee_choices_1, ee_choices_2, ee_choices_3, ee_choices_4, ee_choices_5, ee_choices_6, ee_choices_7, ee_choices_8, ee_choices_9, ee_choices_10,]
            me_choices = [me_choices_1, me_choices_2, me_choices_3, me_choices_4, me_choices_5, me_choices_6, me_choices_7, me_choices_8, me_choices_9, me_choices_10,]
            course_choices = [ge_choices, ce_choices, ece_choices, ee_choices, me_choices]

            def create_choices(choices, questions):
                for i in range(len(choices)):            
                    for j in range(len(choices[i])):
                        Answer.objects.create(content=choices[i][j], question=questions[i])
                print(f'{questions[i].category} choices done')
            # for i in range(len(ce_choices)):
            #         for j in range(len(ce_choices_1)):
            #             Answer.objects.create(content=ce_choices[i][j], question=ce_questions[i])
            
            all_questions = [ge_questions, ce_questions, ece_questions, ee_questions, me_questions]
            [create_choices(choices=choices, questions=questions) for choices, questions in zip(course_choices, all_questions)]

            correct_answers = [
            'quadrant 3',
            'derivative',
            'acceleration',    
            'Integral Calculus and Differential Calculus',
            'Power',
            'increases',
            'Newton’s First Law',
            'Kinetic Energy',
            'Potential Energy',
            '300000000 m/s',
            'mean',
            '8',
            'Force',
            'Scalar Quantity',
            'Displacement',
            '0',
            'Functions',
            'Repel; Attract',
            'Moles',
            'Metalloids',    
            '3 cm',
            'Resultant Force',
            'Normal Force',
            'Pin Support',
            'Fixed Support',
            'Gravel',
            'Sand',
            'A',
            '– 9.81 m/s2',
            'Free Body Diagram',
            'Parallel',
            'Directly Proportional',
            'Current',
            'Voltage',
            '0',
            'Electrical Energy',
            '220 V AC RMS',
            'Battery',
            'Inductor',
            'Kirchhoff’s Voltage Law',
            'breadboard',
            'Directly Proportional',
            '123 cm and 492 cm',
            'Noise',
            'RJ45',
            'Bus Topology',
            'Sinusoidal Wave',
            'Capacitor',
            '1.6 A',
            'Frequency',
            'Gears',
            'Heat Transfer',
            'Mechanical vibration',
            '20-22 °C',
            'Positive',
            'CH4',
            'C',
            '0°C',
            'Damped Vibrations',
            'Vibrations caused due to known exciting force.',
            ]
            
            # repeated_choices = list(Answer.objects.filter(content__in=correct_answers).values('content').annotate(Count('id')).order_by().filter(id__count__gt=1).values_list('content', flat='true'))
            questions = MCQuestion.objects.all()
            for i in range(len(questions)):
                if i == 0:
                    x = 1
                else:
                    x += 4
                questions[i].answer_set.filter(id=x).update(correct=True)
            
        assessment_skills()
        exam_content()

        User.objects.create_superuser('admin','a@a.com','123')
        print('\n ADMIN ACCOUNT \n username:admin \n password:123 \n')
        
        self.stdout.write("Migrating done")