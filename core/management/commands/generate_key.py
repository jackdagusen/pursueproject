from django.core.management.utils import get_random_secret_key  
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'Generate secret key'  
    def handle(self, *args, **options):
    
        secret_key = get_random_secret_key()
        self.stdout.write(f"{secret_key}")