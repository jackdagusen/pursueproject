from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.urls import path, include, reverse_lazy

from register import views as v
from quiz import views 


urlpatterns = [
#ADMIN AND HOME URLS
    path('admin/', admin.site.urls),
    path('login/', v.login, name='login'),
    path("", views.home, name = "home"),
    path('', include('django.contrib.auth.urls')),
    path("home/",views.home, name = "home"),
    path("about/",views.about, name = "about"),
    path("faq/",views.faq, name = "faq"),
    path("progs/",views.progs, name = "progs"),
    path("howto/",views.howto, name = "howto"),
    path("privacy/",views.privacy, name = "privacy"),
    path("terms/",views.terms, name = "terms"),
    path("output/",views.output, name = "output"),
    path("pursueproject/",views.pursueproject, name = "pursueproject"),
    path('pdf/', v.generate_apostille, name='generate_pdf'),
    path('view/pdf/', v.view_apostille, name='view_pdf'),
    path('certificate/', v.generate_certificate, name='generate_certificate'),
    path('view/certificate/', v.view_certificate, name='view_certificate'),


#PROGS
    path("progs/CE",views.CE, name = "CE"),
    path("progs/ECE",views.ECE, name = "ECE"),
    path("progs/ME",views.ME, name = "ME"),
    path("progs/EE",views.EE, name = "EE"),



#PROFILE URLS
    path("register/", v.register, name = "register"),
    path("profile/<str:pk>/", v.profile, name = "profile"),
    path("results/<str:pk>/", v.results, name = "results"),
    path("profile/edit/<str:pk>/", v.edit, name = "edit"),


#PASSWORD URLS
    path("password/", v.change_password, name = 'change_password'),

    path("password_reset/",auth_views.PasswordResetView.as_view(
        template_name = 'password_reset_form.html', 
        success_url = reverse_lazy("password_reset_done"),
        email_template_name = 'password_reset_email.html'),
        name = "password_reset"),
    
    path("password_reset/done/", auth_views.PasswordResetDoneView.as_view(
        template_name = 'registration/password_reset_done.html'),
        name = "password_reset_done"),

    path("password_reset/<uidb64>/<token>/", auth_views.PasswordResetConfirmView.as_view(
        template_name = 'registration/password_reset_confirm.html',
        success_url = reverse_lazy("password_reset_complete")), 
        name = "password_reset_confirm"),

    # path('password_reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
    #     template_name='password_reset_confirm.html'),
    #     name="password_reset_confirm"),

    path("password_reset/complete/", auth_views.PasswordResetCompleteView.as_view(
        template_name = 'registration/password_reset_complete.html'),
        name = "password_reset_complete"),

#python -m smtpd -n -c DebuggingServer localhost:1025
##^this is for setting up the email server for password reset, run in a separate workspace

#QUIZ URL
    path(r'exam/', include('quiz.urls')),

#INPUT GRADES URL
    path('', include('inputshs.urls')),
    

#ASSESSMENT URL
    path('', include('likert.urls')),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)