from django.conf.urls import url
from django.urls import path, include, reverse_lazy

from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import renderers
from rest_framework.routers import DefaultRouter

from .views import AssessmentView

urlpatterns = [
    path('assessment/<str:pk>/', AssessmentView.as_view(), name = "assessment"),
]