from django.contrib import admin
from django.contrib.admin.options import InlineModelAdmin
from likert.models import Assessment, Scale, Question
# from likert.forms import ScaleForm
# Register your models here.


class ScaleInline(admin.StackedInline):
    model = Scale
    # form = ScaleForm


@admin.register(Scale)
class ScaleAdmin(admin.ModelAdmin):
    list_display = [
        'value',
        'question',
        'id',
        'user',
        'date_created',
        'date_updated',
    ]
    list_filter = [
        'user'
    ]


class QuestionInline(admin.StackedInline):
    model = Question
    ordering = ('id',)
    extra = 1


@admin.register(Assessment)
class AssessmentAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'title',
        'date_created',
    ]
    fields = [
        'title',
    ]
    readonly_fields = [
        'id',
        'date_created',
    ]
    inlines = [QuestionInline,]


@admin.register(Question)
class AssessmentQuestionAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'question',
        'assessment',
        'date_created',
    ]
    fields = [
        'question',
        'assessment',
    ]
    list_filter = [
        'assessment',
        'question',
    ]
    search_fields = [
        'assessment',
        'question',
        'id',
    ]
    readonly_fields = [
        'id',
        'date_created',
    ]


# @admin.register(Response)
# class ResponseAdmin(admin.ModelAdmin):
#     list_display = [
#         'id',
#         'user',
#         'assessment',
#         'date_created',
#         "date_updated",
#     ]
#     list_filter = [
#         'user',
#         'assessment__id',
#     ]
#     readonly_fields = (
#         "date_created",
#         "date_updated",
#         "id",
#         "user"
#     )

