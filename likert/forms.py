from django import forms
from likert.models import Scale, Question
from django.utils.safestring import mark_safe


class ScaleForm(forms.ModelForm):
    
    class Meta:
        model = Scale
        fields = ['value',]

    SCALES = (
    ('1','Poor'),
    ('2','Fair'),
    ('3','Good'),
    ('4','Excellent'),
    )
    value = forms.ChoiceField(
        required=True, 
        choices=SCALES,
        widget = forms.RadioSelect(attrs={'class': "custom-radio-list",},
                                    
        ))