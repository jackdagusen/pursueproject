from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.forms.models import inlineformset_factory

from django.shortcuts import redirect
from django.shortcuts import render
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer


from .forms import ScaleForm
from .models import Assessment, Question, Scale, apply_algorithm
from register.models import User
from django.contrib import messages

# Create your views here.

def update_status(user):
    id = User.objects.filter(pk=user.id).values_list('id',flat=True).first()
    obj = User.objects.get(pk=id)
    obj.assessment_done = True
    obj.save()


class AssessmentView(generics.ListCreateAPIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'assessment.html'
    permission_classes = (permissions.IsAuthenticated,)
    
    def get(self, request, pk):
    
        question = Question.objects.filter(assessment_id=1)
        scales = Scale.objects.filter(user_id=self.request.user)
        ScaleFormSet = inlineformset_factory(User, Scale, fields=('value',), form = ScaleForm, \
                                            min_num=1, validate_min=True, max_num=30, validate_max=True, extra=0)
        formset = ScaleFormSet(instance=request.user)
        if request.user.assessment_done:
            messages.error(request, 'You can only answer the assessment test once.')
            return redirect('home')
        context = {'formset': formset, 'question': question}
        return Response(context)

    def post(self, request, *args, **kwargs):
        question = Question.objects.all()
        ScaleFormSet = inlineformset_factory(User, Scale, fields=('value',), form=ScaleForm)
        formset = ScaleFormSet(request.POST, instance=request.user)
        context = {'formset': formset, 'question': question}

        if formset.is_valid():
            if request.user.assessment_done:
                raise PermissionDenied("You can only answer the assessment test once.")
            formset.user = request.user
            formset.save()
            messages.success(request, 'Submitted Successfully.')
            apply_algorithm(self)
            update_status(request.user)
            return redirect('/home')
        else:
            messages.error(request, 'You overlooked an item/s that required an answer.')

        return Response(context)