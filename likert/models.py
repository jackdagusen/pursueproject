from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import query
from django.db.models.signals import post_save
from django.dispatch import receiver

from quiz.models import Updated
from register.models import User
# Create your models here.


class Assessment(Updated):
    title = models.CharField(max_length=255, default=("New Assessment"), verbose_name=("Assessment Title"))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Assessment'


class Question(Updated):
    question = models.TextField(max_length=256)
    description = models.TextField(max_length=256)
    assessment = models.ForeignKey(Assessment, related_name ='question', on_delete=models.DO_NOTHING)
    is_active = models.BooleanField(default=False, verbose_name=("Active Status"))

    def __str__(self):
        return self.question


class Scale(Updated):
    class Value(models.IntegerChoices):
        Very_Poor = 1
        Poor = 2
        Fair = 3
        Excellent = 4

    value = models.IntegerField(choices=Value.choices, null=True,blank=True)
    user = models.ForeignKey(User, related_name='assessment', on_delete=models.CASCADE, null=True, blank=True)
    question = models.ForeignKey(Question, related_name='answer', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.value)
    
    @receiver(post_save, sender=User)
    def answer_create(sender, instance=None, created=False, **kwargs):
        try:
            quantity = Question.objects.filter(assessment__id=1)
        except ObjectDoesNotExist:
            print("No Question matches the given query.")
        if created:
            for question in range(1,len(quantity)+1):
                Scale.objects.create(user=instance,question_id=question)

    # SCALES = (
    #     ('Strongly Disagree','Strongly Disagree'),
    #     ('Somewhat Disagree','Somewhat Disagree'),
    #     ('Neither Agree or Disagree', 'Neither Agree or Disagree'),
    #     ('Somewhat Agree','Somewhat Agree'),
    #     ('Strongly Agree','Strongly Agree'),
    #     ('Very Poor','Very Poor'),
    #     ('Poor','Poor'),
    #     ('Fair','Fair'),
    #     ('Good','Good'),
    #     ('Excellent','Excellent'),
    #     ('Unimportant','Unimportant'),
    #     ('Slightly Important','Slightly Important'),
    #     ('Moderately Important','Moderately Important'),
    #     ('Important','Important'),
    #     ('Very Important','Very Important'),
    # )

def get_total_weighted(weighted, total_weight, assessment_weight):
        total_weighted = (sum(weighted)/total_weight)*assessment_weight
        return total_weighted

def apply_algorithm(self):
    CE = [0.0302619998646, 0.03676122131203, 0.025184483108794, 0.035102565838467, 0.037573623992959, 0.037505923769549, 0.035711867849164, 0.039367679913344, 0.037709024439781, 0.037878274998308, 0.037979825333424, 0.02755399092817, 0.035407216843816, 0.032868458465913, 0.028975695619796, 0.037776724663191, 0.034899465168235, 0.031277503215761, 0.035441066955521, 0.040112382370862, 0.030465100534832, 0.030465100534832, 0.030465100534832, 0.030465100534832, 0.030465100534832, 0.030465100534832, 0.030465100534832, 0.030465100534832, 0.030465100534832, 0.030465100534832]
    ECE = [0.034707589327656, 0.034937911688424, 0.030291655669959, 0.033752178053357, 0.038898887596455, 0.039004096576065, 0.034434614677856, 0.034588162918368, 0.037076213111855, 0.038503643051433, 0.036271506592133, 0.030277438240282, 0.033038463083568, 0.028895504075671, 0.028713520975804, 0.038640130376332, 0.036211793387489, 0.034465893023146, 0.034738867672945, 0.038867609251166, 0.030368429790215, 0.030368429790215, 0.030368429790215, 0.030368429790215, 0.030368429790215, 0.030368429790215, 0.030368429790215, 0.030368429790215, 0.030368429790215, 0.030368429790215]
    EE = [0.032654254800663, 0.037365565194788, 0.034863696916529, 0.035870942586997, 0.036943171849108, 0.037593007765539, 0.035870942586997, 0.034116385612633, 0.035675991812068, 0.037690483153004, 0.037593007765539, 0.030639763459727, 0.033206615329629, 0.033791467654417, 0.030899697826299, 0.037528024173896, 0.036455794911785, 0.035286090262209, 0.035351073853852, 0.038177860090327, 0.0292426162394, 0.0292426162394, 0.0292426162394, 0.0292426162394, 0.0292426162394, 0.0292426162394, 0.0292426162394, 0.0292426162394, 0.0292426162394, 0.0292426162394]
    ME = [0.02817558104204, 0.033180979105495, 0.035619614083116, 0.034968471238101, 0.03597879287827, 0.037955326482009, 0.037850303442491, 0.034842443590679, 0.035577604867309, 0.037577243539742, 0.03547258182779, 0.033183079566286, 0.036566921899574, 0.033517052831955, 0.03213074871031, 0.038039344913624, 0.034695411335353, 0.036188838957307, 0.035451577219887, 0.038438432463794, 0.029458962584957, 0.029458962584957, 0.029458962584957, 0.029458962584957, 0.029458962584957, 0.029458962584957, 0.029458962584957, 0.029458962584957, 0.029458962584957, 0.029458962584957]
    courses = [CE, ECE, EE, ME]
    scales = list(Scale.objects.filter(user__id=self.request.user.id).values_list('value', flat=True))
    user = User.objects.get(id=self.request.user.id)
    for course in courses:
        weighted = []
        for i in range(0,30):
            weighted.append(scales[i]*course[i])
            if course == CE:
                total_weighted = get_total_weighted(weighted=weighted, total_weight=4.0000000000000195, assessment_weight=0.33112799)
                user.results.filter(user=self.request.user).update(CE_assessment=total_weighted)
            if course == ECE:
                total_weighted = get_total_weighted(weighted=weighted, total_weight=3.999999909008457, assessment_weight=0.359096661)

                user.results.filter(user=self.request.user).update(ECE_assessment=total_weighted)
            if course == EE:
                total_weighted = get_total_weighted(weighted=weighted, total_weight=4.000000000000022, assessment_weight=0.413196028)

                user.results.filter(user=self.request.user).update(EE_assessment=total_weighted)
            if course == ME:
                total_weighted = get_total_weighted(weighted=weighted, total_weight=3.999999903378806, assessment_weight=0.322742054)

                user.results.filter(user=self.request.user).update(ME_assessment=total_weighted)

# user = User.objects.get(username='admin')
# user.results.get(user__username='admin').update(ECE_assessment=44)

# >>> user = User.objects.get(username='admin')
# >>> user.results.get(user__username='admin')
# <Results: Results object (1)>
# >>> user = user.results.get(user__username='admin')