from rest_framework import serializers
from .models import Assessment, Question, Scale



class ScaleSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Scale
        fields = ['value', 'user']
        
        
class QuestionSerializer(serializers.ModelSerializer):
    scale = ScaleSerializer(read_only=False)
    
    class Meta:
        model = Question
        fields = ['id', 'question', 'scale']


class AssessmentSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = Assessment
        fields = ['title', 'question']


