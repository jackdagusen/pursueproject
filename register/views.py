import sys
import inspect
from django.contrib import auth, messages
from django.contrib.auth import authenticate, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm, AuthenticationForm
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied, ValidationError
from django.http.response import HttpResponse
from django.shortcuts import render, redirect

from Naked.toolshed.shell import execute_js, muterun_js

from django.template.loader import render_to_string
from weasyprint import HTML, CSS
import tempfile

from inputshs.models import Grade
from register.models import Results, User

from .forms import RegisterForm, EditProfileForm, LoginForm, PasswordChangingForm
from .utils import render_to_pdf

# Create your views here.

def retrieve_name(var):
    callers_local_vars = inspect.currentframe().f_back.f_locals.items()
    return [var_name for var_name, var_val in callers_local_vars if var_val == var]


@login_required
def generate_apostille(request):
    """Generate pdf."""
    # Model data
    user = request.user
    user_results = Results.objects.filter(user_id=user.id)
    individual_result = user_results.first()
    grade = exam_scores = results = context = None

    if user.assessment_done and user.exam_done and user.grades_done:
        try:
            grade = Grade.objects.filter(user=request.user).first()
        except ObjectDoesNotExist:
            grade = None

        def get_score(course):
            try:
                score = user.sitting_set.filter(quiz__title=course).first().current_score
            except (TypeError, AttributeError):
                score = ''
            return score

        GE_score = get_score(course='General Engineering')
        CE_score = get_score(course='Civil Engineering')
        ECE_score = get_score(course='Electronics Engineering')
        EE_score = get_score(course='Electrical Engineering')
        ME_score = get_score(course='Mechanical Engineering')
        exam_scores = [GE_score, CE_score, ECE_score, EE_score, ME_score]

        try:
            results_unsorted = []
            programs = []
            Civil_Engineering =  "{:.2%}".format(individual_result.CE_grades + individual_result.CE_assessment + individual_result.CE_exam)
            Electronics_Engineering =  "{:.2%}".format(individual_result.ECE_grades + individual_result.ECE_assessment + individual_result.ECE_exam)
            Electrical_Engineering =  "{:.2%}".format(individual_result.EE_grades + individual_result.EE_assessment + individual_result.EE_exam)
            Mechanical_Engineering =  "{:.2%}".format(individual_result.ME_grades + individual_result.ME_assessment + individual_result.ME_exam)
            results_unsorted.extend([Civil_Engineering, Electronics_Engineering, Electrical_Engineering, Mechanical_Engineering])
            results = sorted(results_unsorted, reverse=True)
            
            for i in range(len(results)):
                programs.append(str(retrieve_name(results[i]))[2:-2].replace("_", " "))

        except ObjectDoesNotExist:
            results = None

        if not user.program:
            print('test')
            id = User.objects.filter(pk=user.id).values_list('id',flat=True).first()
            obj = User.objects.get(pk=id)
            obj.program = programs[0]
            obj.program_value = results[0]
            obj.save()


        context = {"user": user, "grade": grade, "exam_scores": exam_scores, "results": results, 'programs': programs}

        # Rendered
        html_string = render_to_string('results.html', context)
        html = HTML(string=html_string, base_url=request.build_absolute_uri())
        css = CSS('register/static/assets/bootstrap/css/foroutput.css')
        html.write_pdf('nem/results_pdf/' + 'PURSUE_results.pdf', stylesheets=[css], presentational_hints=True)
        result = html.write_pdf(stylesheets=[css], presentational_hints=True)
        filename = f"{user.username}_Recommendation.pdf"
        
        # Creating http response
        content = "attachment; filename=%s" %(filename)
        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = content
        response['Content-Transfer-Encoding'] = 'binary'

        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(result)
            output.flush()
            output.seek(0)

            if user.hash == None:
                user_object = User.objects.filter(pk=request.user.id)
                apostille = muterun_js('nem/apostille.js')
                if apostille.exitcode == 0:
                    transaction_hash = str(apostille.stdout)[2:-3]
                    print(transaction_hash)
                    if len(transaction_hash) <= 64:
                        user_object.update(hash=transaction_hash)

                else:
                    sys.stderr.write(str(apostille.stderr))

            response.write(output.read())

    else:
        messages.error(request, 'Finish Exams and Assessment Test first.')
        return redirect('/home')

    return response


@login_required
def view_apostille(request):
    """Generate pdf."""
    # Model data
    user = request.user
    user_results = Results.objects.filter(user_id=user.id)
    individual_result = user_results.first()
    grade = exam_scores = results = context = programs = None

    if user.assessment_done and user.exam_done and user.grades_done:
        try:
            grade = Grade.objects.filter(user=request.user).first()
        except ObjectDoesNotExist:
            grade = None

        def get_score(course):
            try:
                score = user.sitting_set.filter(quiz__title=course).first().current_score
            except (TypeError, AttributeError):
                score = ''
            return score

        GE_score = get_score(course='General Engineering')
        CE_score = get_score(course='Civil Engineering')
        ECE_score = get_score(course='Electronics Engineering')
        EE_score = get_score(course='Electrical Engineering')
        ME_score = get_score(course='Mechanical Engineering')
        exam_scores = [GE_score, CE_score, ECE_score, EE_score, ME_score]

        try:
            results_unsorted = []
            programs = []
            Civil_Engineering =  "{:.2%}".format(individual_result.CE_grades + individual_result.CE_assessment + individual_result.CE_exam)
            Electronics_Engineering =  "{:.2%}".format(individual_result.ECE_grades + individual_result.ECE_assessment + individual_result.ECE_exam)
            Electrical_Engineering =  "{:.2%}".format(individual_result.EE_grades + individual_result.EE_assessment + individual_result.EE_exam)
            Mechanical_Engineering =  "{:.2%}".format(individual_result.ME_grades + individual_result.ME_assessment + individual_result.ME_exam)
            results_unsorted.extend([Civil_Engineering, Electronics_Engineering, Electrical_Engineering, Mechanical_Engineering])
            results = sorted(results_unsorted, reverse=True)
            
            for i in range(len(results)):
                programs.append(str(retrieve_name(results[i]))[2:-2].replace("_", " "))
        except ObjectDoesNotExist:
            results = None

        if not user.program:
            print('test')
            id = User.objects.filter(pk=user.id).values_list('id',flat=True).first()
            obj = User.objects.get(pk=id)
            obj.program = programs[0]
            obj.program_value = results[0]
            obj.save()
            

        context = {"user": user, "grade": grade, "exam_scores": exam_scores, "results": results, "programs": programs}

        # Rendered
        html_string = render_to_string('results.html', context)
        html = HTML(string=html_string, base_url=request.build_absolute_uri())
        css = CSS('register/static/assets/bootstrap/css/foroutput.css')
        html.write_pdf('nem/results_pdf/' + 'PURSUE_results.pdf', stylesheets=[css], presentational_hints=True)
        result = html.write_pdf(stylesheets=[css], presentational_hints=True)
        filename = f"{user.username}_Recommendation.pdf"
        
        # Creating http response
        content = "inline; filename=%s" %(filename)
        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = content
        response['Content-Transfer-Encoding'] = 'binary'

        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(result)
            output.flush()
            output.seek(0)

            if user.hash == None:
                user_object = User.objects.filter(pk=request.user.id)
                apostille = muterun_js('nem/apostille.js')
                if apostille.exitcode == 0:
                    transaction_hash = str(apostille.stdout)[2:-3]
                    print(transaction_hash)
                    if len(transaction_hash) <= 64:
                        user_object.update(hash=transaction_hash)

                else:
                    sys.stderr.write(str(apostille.stderr))

            response.write(output.read())

    else:
        messages.error(request, 'Finish Exams and Assessment Test first.')
        return redirect('/home')

    return response

@login_required
def generate_certificate(request):
    """Generate pdf."""
    # Model data
    user = request.user
    context = None

    if user.assessment_done and user.exam_done and user.grades_done and user.hash and user.program:

        context = {"user": user}

        # Rendered
        html_string = render_to_string('certificate.html', context)
        html = HTML(string=html_string, base_url=request.build_absolute_uri())
        css = CSS('register/static/assets/bootstrap/css/foroutput.css')
        html.write_pdf('nem/results_pdf/' + 'PURSUE_results.pdf', stylesheets=[css], presentational_hints=True)
        result = html.write_pdf(stylesheets=[css], presentational_hints=True)
        filename = f"{user.username}_Certificate.pdf"
        
        # Creating http response
        content = "attachment; filename=%s" %(filename)
        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = content
        response['Content-Transfer-Encoding'] = 'binary'

        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(result)
            output.flush()
            output.seek(0)
            response.write(output.read())


    else:
        messages.error(request, 'Finish generating PDF first.')
        return redirect('/output')

    return response

@login_required
def view_certificate(request):
    user = request.user
    context = None

    if user.assessment_done and user.exam_done and user.grades_done and user.hash:
        context = {"user": user}

        # Rendered
        html_string = render_to_string('certificate.html', context)
        html = HTML(string=html_string, base_url=request.build_absolute_uri())
        css = CSS('register/static/assets/bootstrap/css/foroutput.css')
        html.write_pdf('nem/results_pdf/' + 'PURSUE_results.pdf', stylesheets=[css], presentational_hints=True)
        result = html.write_pdf(stylesheets=[css], presentational_hints=True)
        filename = f"{user.username}_Certificate.pdf"
        
        # Creating http response
        content = "inline; filename=%s" %(filename)
        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = content
        response['Content-Transfer-Encoding'] = 'binary'

        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(result)
            output.flush()
            output.seek(0)
            response.write(output.read())


    else:
        messages.error(request, 'Finish generating PDF first.')
        return redirect('/output')

    return response


def register(response):
    if response.user.is_authenticated:
        return redirect('/home')
    if response.method == "POST":
        form =  RegisterForm(response.POST)
        if form.is_valid() == True:     
            form.save()         
            messages.success(response, 'Account Created Successfully.')
            return redirect("/home")  

    else:
        form = RegisterForm()  

    return render(response, "register/register.html", {"form":form})


#login_required ay nagsasabi need muna mag login bago ma access etong feature na to, 
# if hindi logged in, magrrefresh lang yung page
@login_required
def profile(request, pk):
    user = request.user
    user_results = Results.objects.filter(user_id=user.id)
    individual_result = user_results.first()
    grade = exam_scores = context = None
    try:
        grade = Grade.objects.filter(user=request.user).first()
    except ObjectDoesNotExist:
        grade = None

    def get_score(course):
        try:
            score = user.sitting_set.filter(quiz__title=course).first().current_score
        except (TypeError, AttributeError):
            score = ''
        return score

    GE_score = get_score(course='General Engineering')
    CE_score = get_score(course='Civil Engineering')
    ECE_score = get_score(course='Electronics Engineering')
    EE_score = get_score(course='Electrical Engineering')
    ME_score = get_score(course='Mechanical Engineering')
    exam_scores = [GE_score, CE_score, ECE_score, EE_score, ME_score]
    try:
        results_unsorted = []
        programs = []
        Civil_Engineering = "{:.2%}".format(
            individual_result.CE_grades + individual_result.CE_assessment + individual_result.CE_exam)
        Electronics_Engineering = "{:.2%}".format(
            individual_result.ECE_grades + individual_result.ECE_assessment + individual_result.ECE_exam)
        Electrical_Engineering = "{:.2%}".format(
            individual_result.EE_grades + individual_result.EE_assessment + individual_result.EE_exam)
        Mechanical_Engineering = "{:.2%}".format(
            individual_result.ME_grades + individual_result.ME_assessment + individual_result.ME_exam)
        results_unsorted.extend(
            [Civil_Engineering, Electronics_Engineering, Electrical_Engineering, Mechanical_Engineering])
        results = sorted(results_unsorted, reverse=True)

        for i in range(len(results)):
            programs.append(str(retrieve_name(results[i]))[2:-2].replace("_", " "))
    except ObjectDoesNotExist:
        results = None

    context = {"user": user, "grade": grade, "exam_scores": exam_scores, "results": results, "programs": programs}

    return render(request, 'profile.html', context)


@login_required
def results(request, pk):
    user = request.user
    user_results = Results.objects.filter(user_id=user.id)
    individual_result = user_results.first()
    grade = exam_scores = results = programs = context = None

    try:
        grade = Grade.objects.filter(user=request.user).first()
    except ObjectDoesNotExist:
        grade = None

    if user.assessment_done and user.exam_done and individual_result.ME_exam_done and user.grades_done:
        try:
            results_unsorted = []
            programs = []
            Civil_Engineering =  "{:.2%}".format(individual_result.CE_grades + individual_result.CE_assessment + individual_result.CE_exam)
            Electronics_Engineering =  "{:.2%}".format(individual_result.ECE_grades + individual_result.ECE_assessment + individual_result.ECE_exam)
            Electrical_Engineering =  "{:.2%}".format(individual_result.EE_grades + individual_result.EE_assessment + individual_result.EE_exam)
            Mechanical_Engineering =  "{:.2%}".format(individual_result.ME_grades + individual_result.ME_assessment + individual_result.ME_exam)
            results_unsorted.extend([Civil_Engineering, Electronics_Engineering, Electrical_Engineering, Mechanical_Engineering])
            results = sorted(results_unsorted, reverse=True)
            
            for i in range(len(results)):
                programs.append(str(retrieve_name(results[i]))[2:-2].replace("_", " "))

        except ObjectDoesNotExist:
            results = None

    def get_score(course):
        try:
            score = user.sitting_set.filter(quiz__title=course).first().current_score
        except (TypeError, AttributeError):
            score = ''
        return score

    GE_score = get_score(course='General Engineering')
    CE_score = get_score(course='Civil Engineering')
    ECE_score = get_score(course='Electronics Engineering')
    EE_score = get_score(course='Electrical Engineering')
    ME_score = get_score(course='Mechanical Engineering')
    exam_scores = [GE_score, CE_score, ECE_score, EE_score, ME_score]
    context = {"user": user, "grade": grade, "exam_scores": exam_scores, "results": results, 'programs': programs}

    return render(request, 'results.html', context)


@login_required
def edit(request, pk):
    user = User.objects.get(id=request.user.id)
    initial_dict = {
        "username": user.username,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "school": user.school,
    }
    form = EditProfileForm(request.POST, instance = request.user, initial = initial_dict)


    if request.method == "POST":
        form = EditProfileForm(request.POST, instance = request.user, initial = initial_dict)
        if form.is_valid():
            if sum(x == '' for x in form.cleaned_data.values()) == 5:
                messages.error(request, 'Edit at least one field.')
                args = {"form": form, "user":user}
                return render(request, 'edit.html', args)
            obj = form.save(commit=False)   
            if not form.data['first_name']:
                obj.first_name = user.first_name
            if not form.data['last_name']:
                obj.last_name = user.last_name
            if not form.data['username']:
                obj.username = user.username
            if not form.data['email']:
                obj.email = user.email
            if not form.data['school']:
                obj.school = user.school
            obj.save()
            messages.success(request, 'Profile Updated Successfully.')
            return redirect(f'/profile/{request.user.id}')
        
        args = {"form": form, "user":user}
        return render(request, 'edit.html', args)
    form = EditProfileForm(instance = request.user, initial = initial_dict)
    args = {"form": form, "user":user}
    return render(request, 'edit.html', args)
    

def login(request):
    if request.user.is_authenticated:
        return redirect('/home')
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request, data=request.POST)
        username=request.POST['username']
        password=request.POST['password']
        if form.is_valid():
            user=auth.authenticate(username=username,password=password)
            if user is not None:
                auth.login(request, user)
                if user.grades_done == False:
                    return redirect(f'/grades/{user.id}/')
                else:
                    return redirect('/')
            else:
                messages.info(request, "Password didn't match")
                return render(request,'registration/login.html', {"form": form})
    return render(request,'registration/login.html', {"form": form})


@login_required     
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangingForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Password Updated Successfully.')
            return redirect(f'/profile/{request.user.id}')
        else:
            messages.error(request, 'Please correct the following error.')
    else:
        form = PasswordChangingForm(request.user)
    return render(request, 'change_password.html', {"form": form})




# class UpdatedLoginView(LoginView):
#     form_class = LoginForm
    
#     def form_valid(self, form):

#         remember_me = form.cleaned_data['formCheck-1']  # get remember me data from cleaned_data of form
#         if not remember_me:
#             self.request.session.set_expiry(0)  # if remember me is 
#             self.request.session.modified = True
#         return super(UpdatedLoginView, self).form_valid(form)



#python -m smtpd -n -c DebuggingServer localhost:1025
