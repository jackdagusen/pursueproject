from django.core.validators import RegexValidator

alphabetic = RegexValidator(r'^[ a-zA-Z]*$', 'Only alphabetic characters are allowed.')

