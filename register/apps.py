from django.apps import AppConfig

#To be added to settings.py for code to work
class RegisterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'register'
