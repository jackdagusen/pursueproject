import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser, User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from .validators import alphabetic

class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(_('first name'), max_length=150, blank=True, validators=[alphabetic])
    last_name = models.CharField(_('last name'), max_length=150, blank=True, validators=[alphabetic])
    date_created = models.DateTimeField(auto_now_add = True)
    grades_done = models.BooleanField(default=False)
    exam_done = models.BooleanField(default=False)
    assessment_done = models.BooleanField(default=False)
    email = models.EmailField(unique=True)
    hash = models.CharField(null=True, blank=True, max_length=64)
    program = models.CharField(null=True, blank=True, max_length=64)
    program_value = models.CharField(null=True, blank=True, max_length=64)
    school = models.CharField(null=True, blank=True, max_length=64)

class Results(models.Model):
    user = models.ForeignKey(User, related_name = 'results', on_delete = models.CASCADE)
    CE_grades = models.FloatField(default=0,  null=True, blank=True)
    ECE_grades = models.FloatField(default=0,  null=True, blank=True)
    EE_grades = models.FloatField(default=0,  null=True, blank=True)
    ME_grades = models.FloatField(default=0,  null=True, blank=True)
    
    GE_exam= models.FloatField(default=0,  null=True, blank=True)
    CE_exam= models.FloatField(default=0,  null=True, blank=True)
    ECE_exam = models.FloatField(default=0,  null=True, blank=True)
    EE_exam = models.FloatField(default=0,  null=True, blank=True)
    ME_exam = models.FloatField(default=0,  null=True, blank=True)

    GE_exam_done = models.BooleanField(default=False, null=True, blank=True)
    CE_exam_done = models.BooleanField(default=False, null=True, blank=True)
    ECE_exam_done = models.BooleanField(default=False, null=True, blank=True)
    EE_exam_done = models.BooleanField(default=False, null=True, blank=True)
    ME_exam_done = models.BooleanField(default=False, null=True, blank=True)

    CE_assessment = models.FloatField(default=0,  null=True, blank=True)
    ECE_assessment = models.FloatField(default=0,  null=True, blank=True)
    EE_assessment = models.FloatField(default=0,  null=True, blank=True)
    ME_assessment = models.FloatField(default=0,  null=True, blank=True)
    
    class Meta:
            verbose_name_plural = ('Results')

    def __str__(self):
        return self.user.username

    @receiver(post_save, sender=User)
    def result_create(sender, instance=None, created=False, **kwargs):
        if created:
            Results.objects.create(user=instance,)



#gawa ng custom user model na boolean field, if true ang "is done with test/grade/quiz", redirect to home