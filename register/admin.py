from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.forms.models import BaseInlineFormSet

from register.models import Results, User

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = [
        'username',
        'email',
        'grades_done',
        'assessment_done',
        'exam_done',
        'program',
        'program_value',
        'hash',
    ]


@admin.register(Results)
class ResultsAdmin(admin.ModelAdmin):
    list_display = [
        'user',
        'CE_grades',
        'ECE_grades',
        'EE_grades',
        'ME_grades',
        'CE_assessment',
        'ECE_assessment',
        'EE_assessment',
        'ME_assessment',
        'CE_exam',
        'ECE_exam',
        'EE_exam',
        'ME_exam',
    ]

    list_filter = [
        'user'
    ]