from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm, AuthenticationForm, PasswordChangeForm
from django.contrib.auth import login, authenticate
from register.models import User
from django.forms.widgets import NumberInput
from inputshs import models


class RegisterForm(UserCreationForm):
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control form-control-user', 'type':'text', 'placeholder':'Email Address'}),required = True)
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control form-control-user', 'type':'text', 'placeholder':'Username'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control form-control-user', 'type':'password', 'placeholder':'Password'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control form-control-user', 'type': 'password', 'placeholder': 'Repeat Password'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-user', 'type': 'text', 'placeholder': 'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-user', 'type': 'text', 'placeholder': 'Last Name'}))
    school = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-user', 'type': 'text', 'placeholder': 'School'}))
    class Meta:
	    model = User    
	    fields = [
            "username",
            "first_name",
            "last_name",
            "email",
            "school",
            "password1",
            "password2",
            ]

    ###errors for email and username
    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            raise forms.ValidationError("Email is already registered")
        return self.cleaned_data['email']

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user

    def clean_username(self):
        if User.objects.filter(username=self.cleaned_data['username']).exists():
            raise forms.ValidationError("Username is already in use")
        return self.cleaned_data['username']

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.username = self.cleaned_data['username']
        if commit:
            user.save()
        return user


#####################

# class LoginForm(AuthenticationForm):
#     remember_me = forms.BooleanField()  # and add the remember_me field


#Eto yung mga forms na pwede nila i edit sa future
class EditProfileForm(UserChangeForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}),required=False)
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}),required=False)
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}),required=False)
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}),required=False)
    school = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}),required=False)

    class Meta:
        model = User
        fields = {
            'username',
            'first_name',
            'last_name',
            'email',
            'school',
        }
        exclude = ('password',)

        def clean_email(self):    
            email = self.cleaned_data["email"]
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with that email already exists.')
            return email.lower()

        def clean_username(self):
            username = self.cleaned_data['username']
            if User.objects.exclude(pk=self.instance.pk).filter(username=username).exists():
                raise forms.ValidationError(u'Username "%s" is already in use.' % username)
            return username

        def save(self, commit=True):
            user = super(EditProfileForm, self).save(commit=False)
            user.email = self.cleaned_data['email']
            user.username = self.cleaned_data['username']
            if commit:
                user.save()
            return user

class PasswordChangingForm(PasswordChangeForm):
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'type':'password', 'placeholder': 'Old Password'}))
    new_password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control form-control-user', 'type':'password', 'placeholder': 'New Password'}))
    new_password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control form-control-user', 'type':'password', 'placeholder': 'Confirm New Password'}))

    class Meta:
        model = User
        fields = {
            'old_password',
            'new_password1',
            'new_password2',
        }

class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control form-control-user', 'type':'text', 'placeholder':'Enter Username ...'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control form-control-user', 'type':'password', 'placeholder':'Password'}))

    class Meta:
        model = User
        fields = {
            'username',
            'password',
        }
