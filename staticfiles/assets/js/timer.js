btnClear.onclick = function () {
      localStorage.clear();
      count_timer = 0;
      document.getElementById ("total-time-left").innerHTML = count_timer;

};

if(localStorage.getItem("count_timer")){
    var count_timer = localStorage.getItem("count_timer");
} else {
    var count_timer = 15;
}
var minutes = parseInt(count_timer/60);
var seconds = parseInt(count_timer%60);
function countDownTimer(){
    if(seconds < 10){
        seconds= "0"+ seconds ;
    }if(minutes < 10){
        minutes= "0"+ minutes ;
    }

    document.getElementById("total-time-left").innerHTML = +minutes+" Minutes "+seconds+" Seconds";
    if(count_timer <= 0){
         document.getElementById("myForm").submit();
         localStorage.clear("count_timer");
    } else {
        count_timer = count_timer -1 ;
        minutes = parseInt(count_timer/60);
        seconds = parseInt(count_timer%60);
        localStorage.setItem("count_timer",count_timer);
        setTimeout("countDownTimer()",1000);
    }
}
setTimeout("countDownTimer()",1000);