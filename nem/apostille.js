const fs = require('fs');
let nem = require("nem-sdk").default;
// let fs = require("fs").default;
var url = "https://nistest.opening-line.jp";
var port = 7891;

let endpoint = nem.model.objects.create("endpoint")(url, port);

let common = nem.model.objects.create('common')('', 'a7ae05d15f860658d8dad6fd26b647d8b0ca8378446cccaeb5a4c740d6c6252d');

var fileName = 'nem/results_pdf/PURSUE_results.pdf'

var fileContent = fs.readFileSync(fileName, 'base64');
// console.log(fileContent)
// var fileContent = nem.crypto.js.enc.Utf8.parse('PURSUE Apostille');

// Create the apostille
var apostille = nem.model.apostille.create(
    common,
    fileName,
    fileContent,
    "PURSUE",
    nem.model.apostille.hashing["SHA256"],
    false,
    "",
    true,
    nem.model.network.data.testnet.id);

// Serialize transfer transaction and announce
nem.model.transactions.send(common, apostille.transaction, endpoint).then(function success(res) {
    console.log(res.transactionHash.data);
}, function error(err) {
    console.log(err)
});