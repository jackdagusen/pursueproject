const fs = require('fs');
let nem = require("nem-sdk").default;
var url = "https://nistest.opening-line.jp";
var port = 7891;

let endpoint = nem.model.objects.create("endpoint")(url, port);

var fileName = './results_pdf/PURSUE_results.pdf'
var fileContent = fs.readFileSync(fileName, 'base64');

var txHash = "b01e7e8166b2b3a40c3fa6805e3f46fbd58df9f164aae16331f067cc58bd81cf";

nem.com.requests.transaction.byHash(endpoint, txHash).then(function(res) {
    // Verify
    if (nem.model.apostille.verify(fileContent, res.transaction)) {
        console.log("Apostille is valid");
    } else {
        console.log("Apostille is invalid");
    }
}, function(err) {
    console.log("Apostille is invalid");
    console.log(err);
});