from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.shortcuts import render

from numpy import asarray,array
import pandas as pd
from tensorflow.keras.models import load_model

from rest_framework import status, generics, permissions, renderers, viewsets
from rest_framework.decorators import api_view, action
from rest_framework.response import Response

from .forms import InputGradesForm
from .models import Grade
from .permissions  import IsOwnerOrReadOnly
from register.models import User
from .serializers import GradesSerializer, UserGradesSerializer


def update_status(user):
    id = User.objects.filter(pk=user.id).values_list('id',flat=True).first()
    obj = User.objects.get(pk=id)
    obj.grades_done = True
    obj.save()

#@api_view(["POST"])
def GradeML(unit):
    try:
        mlpnn = load_model('mlpmodel.h5')
        yhat = mlpnn.predict(unit)
        shsCEraw = (yhat[0][0])
        shsMEraw = (yhat[0][1])
        shsECEraw = (yhat[0][2])
        shsEEraw = (yhat[0][3])
        shsCE = shsCEraw*0.304562467
        shsME = shsMEraw*0.29467961 
        shsECE = shsECEraw*0.249835961 
        shsEE = shsEEraw*0.242074123 
        SHS_Equivalents = [shsCE,shsECE,shsEE,shsME]
        #print (SHS_Equivalents)
        
        return SHS_Equivalents
    except ValueError as e:
        return Response(e.args[0], status.HTTP_400_BAD_REQUEST)


@login_required
def grades(request, pk):
    if request.user.grades_done:
        return redirect('/')
    form = InputGradesForm()
    if request.method == 'POST':
        form = InputGradesForm(request.POST)
        if request.user.grades_done:
            PermissionDenied('You can only input your grades once.')
        if form.is_valid():
            myDict = (request.POST).dict()
            data_list = list(myDict.values())
            data_list.pop(0)
            new_grades = [float(g) for g in data_list]
            newX = asarray([new_grades])
            prediction = GradeML(newX)
            grade = form.save(commit=False)
            grade.user = request.user
            grade.save()
            messages.success(request, 'Submitted Successfully.')
            user = User.objects.get(pk=request.user.id)
            user.results.update(CE_grades=prediction[0], ECE_grades=prediction[1], EE_grades=prediction[2], ME_grades=prediction[3])
            update_status(user=request.user)
            return HttpResponseRedirect('/')

    return render(request, 'grades.html', {'form':form})


class GradeViewSet(viewsets.ModelViewSet):
    queryset = Grade.objects.all()
    serializer_class = GradesSerializer
    permission_classes = [permissions.AllowAny]

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        grade = self.get_object()
        return Response(grade.highlighted)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Grade.objects.all()
    serializer_class = UserGradesSerializer

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        grade = self.get_object()
        return Response(grade.highlighted)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class GradeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Grade.objects.all()
    serializer_class = GradesSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]


# def grades(request, pk):
    # form = InputGradesForm()
    # if request.method == 'POST':
    #     form = InputGradesForm(request.POST)
    #     if form.is_valid():
    #         grade = form.save(commit=False)
    #         grade.user = request.user
    #         grade.save()
    #         User.objects.filter(pk=request.user.id).update(grades_done=True)
    #         return HttpResponseRedirect('/')
    # if request.user.grades_done:
    #     return redirect('/')
    # return render(request, 'grades.html', {'form': form})
