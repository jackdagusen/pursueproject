from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_grade(value):
    if value < 75:
        raise ValidationError(
            _('Input must be a passing grade.')
        )
    if value > 100:
        raise ValidationError(
            _('Input must be at most 100.' )
        )