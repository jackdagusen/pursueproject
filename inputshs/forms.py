from django import forms
from .models import Grade
from django.forms import ModelForm

from .validators import validate_grade

class InputGradesForm(ModelForm):
    class Meta:
        model = Grade
        fields =(
            'General_Math',
            'ICT',
            'Statistics',
            'Pre_Calculus',
            'Basic_Calculus',
            'Biology_1',
            'Biology_2',
            'Chemistry_1',
            'Chemistry_2',
            'Physics_1',
            'Physics_2',
        )

    General_Math = forms.FloatField(label = 'General Math', validators=[validate_grade])
    ICT = forms.FloatField(label = 'Empowerment Technologies and ICT', validators=[validate_grade])
    Statistics = forms.FloatField(label = 'Statistics and Probability', validators=[validate_grade])
    Pre_Calculus = forms.FloatField(label = 'Pre-Calculus', validators=[validate_grade])
    Basic_Calculus = forms.FloatField(label = 'BasicCalculus', validators=[validate_grade])
    Biology_1 = forms.FloatField(label = 'Biology 1', validators=[validate_grade])
    Biology_2 = forms.FloatField(label = 'Biology 2', validators=[validate_grade])
    Chemistry_1 = forms.FloatField(label = 'Chemistry 1', validators=[validate_grade])
    Chemistry_2 = forms.FloatField(label = 'Chemistry 2', validators=[validate_grade])
    Physics_1 =forms.FloatField(label = 'Physics 1', validators=[validate_grade])
    Physics_2 = forms.FloatField(label = 'Physics 2', validators=[validate_grade])