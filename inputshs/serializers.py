from rest_framework import serializers
from .models import Grade
from django.contrib.auth.models import User
from django.db import models

class GradesSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Grade
        fields = [
            # 'id',
            'user',
            'General_Math',
            'ICT',
            'Statistics',
            'Pre_Calculus',
            'Basic_Calculus',
            'Biology_1',
            'Biology_2',
            'Chemistry_1',
            'Chemistry_2',
            'Physics_1',
            'Physics_2',
        ]



class UserGradesSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Grade
        fields = [
            'user',
            'General_Math',
            'ICT',
            'Statistics',
            'Pre_Calculus',
            'Basic_Calculus',
            'Biology_1',
            'Biology_2',
            'Chemistry_1',
            'Chemistry_2',
            'Physics_1',
            'Physics_2',
        ]
