from decimal import Decimal
from register.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _  #_ before objects can be translated

from .validators import validate_grade


class MinMaxFloat(models.FloatField):       # Float with minimum and maximum values

    def __init__(self, min_value=None, max_value=None, *args, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        super(MinMaxFloat, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value' : self.max_value}
        defaults.update(kwargs)
        return super(MinMaxFloat, self).formfield(**defaults)


class Grade(models.Model):
    user = models.ForeignKey(User, related_name = 'grades', on_delete = models.CASCADE)
    created = models.DateTimeField(auto_now_add = True)
    General_Math = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    ICT = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    Statistics = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    Pre_Calculus = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    Basic_Calculus = models.FloatField(default=0, null=True, blank=True, validators=[validate_grade])
    Physics_1 =models.FloatField(default=0,   null=True, blank=True, validators=[validate_grade])
    Physics_2 = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    Chemistry_1 = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    Chemistry_2 = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    Biology_1 = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])
    Biology_2 = models.FloatField(default=0,  null=True, blank=True, validators=[validate_grade])


    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Grade')
        verbose_name_plural = _('Grades')
        ordering = ['created']


    # @receiver(post_save, sender=User)
    # def grade_create(sender, instance=None, created=False, **kwargs):
    #     if created:
    #         Grade.objects.create(user=instance,)


# General_Math=75,
# ICT=75,
# Statistics=75,
# Pre_Calculus=75,
# Basic_Calculus=75,
# Physics_1=75,
# Physics_2=75,
# Chemistry_1=75,
# Chemistry_2=75,
# Biology_1=75,
# Biology_2=75