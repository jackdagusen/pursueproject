from django.contrib import admin
from . import models

@admin.register(models.Grade)
class GradesAdmin(admin.ModelAdmin):
    fields = (
        'General_Math',
        'ICT',
        'Statistics',
        'Pre_Calculus',
        'Basic_Calculus',
        'Physics_1',
        'Physics_2',
        'Chemistry_1',
        'Chemistry_2',
        'Biology_1',
        'Biology_2',
        'user',
                )
    list_display = [
        'user',
        'General_Math',
        'ICT',
        'Statistics',
        'Pre_Calculus',
        'Basic_Calculus',
        'Physics_1',
        'Physics_2',
        'Chemistry_1',
        'Chemistry_2',
        'Biology_1',
        'Biology_2',
        'created'
    ]

    

    

