from django.conf.urls import url
from django.urls import path, include, reverse_lazy

from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import renderers
from rest_framework.routers import DefaultRouter

from .views import GradeViewSet, UserViewSet, grades


router = DefaultRouter()
router.register(r'grades', GradeViewSet, basename = 'grade')
router.register(r'users', UserViewSet, basename = 'user')

#The API URLs are now determined automatically by the router.
urlpatterns = [
    path('grades/<str:pk>/',grades, name = 'grades'),
]

# urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += path('api/', include(router.urls)),






#ETO YUNG LIST LANG NG CODES NA GAGAMITIN IF EVER DI MAG WORK YUNG ROUTER
# grade_list = GradeViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })
# grade_detail = GradeViewSet.as_view({
#     'get': 'retrieve',
#     'put': 'update',
#     'patch': 'partial_update',
#     'delete': 'destroy'
# })
# grade_highlight = GradeViewSet.as_view({
#     'get': 'highlight'
# }, renderer_classes=[renderers.StaticHTMLRenderer])
# user_list = UserViewSet.as_view({
#     'get': 'list'
# })
# user_detail = UserViewSet.as_view({
#     'get': 'retrieve'
# })

# urlpatterns = format_suffix_patterns([
#     # path('inputgrades/', grades_list.as_view(), namespace = 'grade'),
#     path('', views.api_root),
#     path('grades/', grade_list, name = 'grade-list'),
#     path('grades/<int:pk>/', grade_detail, name = 'grade-detail'),
#     path('grades/<int:pk>/highlight/', GradesHighlight.as_view(), name = 'grade-highlight'),

#     path('users/', user_list, name='user-list'),
#     path('users/<int:pk>/', user_detail, name = 'user-detail' ),
#     path('api-auth/', include('rest_framework.urls')),

# ])


