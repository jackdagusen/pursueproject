import random

from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, TemplateView, FormView

from .forms import QuestionForm, EssayForm
from .models import Quiz, Category, Progress, Sitting, Question
from essay.models import Essay_Question
from register.models import User, Results


@login_required
def home(request):
    user = request.user
    if user.grades_done:
        return render(request,"main/home.html", {'user': user})
    else: 
        return HttpResponseRedirect(f'/grades/{user.id}')


def about(response):
    return render(response,"main/about.html", {})


def faq(response):
    return render(response,"main/faq.html", {})


def progs(response):
    return render(response,"main/progs.html", {})


def terms(response):
    return render(response,"main/terms.html", {})


def privacy(response):
    return render(response,"main/privacy.html", {})


def howto(response):
    return render(response,"main/howto.html", {})

def pursueproject(response):
    return render(response,"main/pursueproject.html", {})

@login_required
def output(response):
    return render(response,"main/output.html", {})

#PROGS

def CE(response):
    return render(response,"others/CE.html", {})


def ME(response):
    return render(response,"others/ME.html", {})


def ECE(response):
    return render(response,"others/ECE.html", {})


def EE(response):
    return render(response,"others/EE.html", {})


class QuizMarkerMixin(object):
    @method_decorator(login_required)
    @method_decorator(permission_required('quiz.view_sittings'))
    def dispatch(self, *args, **kwargs):
        return super(QuizMarkerMixin, self).dispatch(*args, **kwargs)


class SittingFilterTitleMixin(object):
    def get_queryset(self):
        queryset = super(SittingFilterTitleMixin, self).get_queryset()
        quiz_filter = self.request.GET.get('quiz_filter')
        if quiz_filter:
            queryset = queryset.filter(quiz__title__icontains=quiz_filter)

        return queryset


class QuizListView(ListView):
    model = Quiz

    def get_context_data(self, **kwargs):
        context = super(QuizListView, self).get_context_data(**kwargs)
        try:
            user = User.objects.get(id=self.request.user.id)
        except ObjectDoesNotExist:
            raise PermissionDenied('Log in first.')
        result = user.results.first()
        try:
            current_exam = user.sitting_set.last()
        except ObjectDoesNotExist:
            current_exam = None

        def get_score(course):
            try:
                score = user.sitting_set.filter(quiz__title=course).first()
            except (TypeError, AttributeError):
                score = ''
            return score

        GE_score = get_score(course='General Engineering')
        CE_score = get_score(course='Civil Engineering')
        ECE_score = get_score(course='Electronics Engineering')
        EE_score = get_score(course='Electrical Engineering')
        ME_score = get_score(course='Mechanical Engineering')
        exam_scores = [GE_score, CE_score, ECE_score, EE_score, ME_score]
        context.update({"exam_scores": exam_scores, "result":result, "current_exam":current_exam})
        return context

    def get_queryset(self):
        queryset = super(QuizListView, self).get_queryset()
        return queryset.filter(draft=False)


class QuizDetailView(DetailView):
    model = Quiz
    slug_field = 'url'

    def get(self, request, *args, **kwargs):
        try:
            user = User.objects.get(id=self.request.user.id)
        except ObjectDoesNotExist:
            raise PermissionDenied('Log in first.')
        try:
            current_exam = user.sitting_set.last()
        except ObjectDoesNotExist:
            current_exam = None
            
        self.object = self.get_object()

        if current_exam is not None:
            if current_exam.complete == False:
                return redirect('quiz_index')

        if self.object.draft and not request.user.has_perm('quiz.change_quiz'):
            raise PermissionDenied

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class CategoriesListView(ListView):
    model = Category


class ViewQuizListByCategory(ListView):
    model = Quiz
    template_name = 'view_quiz_category.html'

    def dispatch(self, request, *args, **kwargs):
        self.category = get_object_or_404(
            Category,
            category=self.kwargs['category_name']
        )

        return super(ViewQuizListByCategory, self).\
            dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ViewQuizListByCategory, self)\
            .get_context_data(**kwargs)

        context['category'] = self.category
        return context

    def get_queryset(self):
        queryset = super(ViewQuizListByCategory, self).get_queryset()
        return queryset.filter(category=self.category, draft=False)


class QuizUserProgressView(TemplateView):
    template_name = 'progress.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(QuizUserProgressView, self)\
            .dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(QuizUserProgressView, self).get_context_data(**kwargs)
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        context['cat_scores'] = progress.list_all_cat_scores
        context['exams'] = progress.show_exams()
        return context


class QuizMarkingList(QuizMarkerMixin, SittingFilterTitleMixin, ListView):
    model = Sitting

    def get_queryset(self):
        queryset = super(QuizMarkingList, self).get_queryset()\
                                            .filter(complete=True)

        user_filter = self.request.GET.get('user_filter')
        if user_filter:
            queryset = queryset.filter(user__username__icontains=user_filter)

        return queryset


class QuizMarkingDetail(QuizMarkerMixin, DetailView):
    model = Sitting

    def post(self, request, *args, **kwargs):
        sitting = self.get_object()

        q_to_toggle = request.POST.get('qid', None)
        if q_to_toggle:
            q = Question.objects.get_subclass(id=int(q_to_toggle))
            if int(q_to_toggle) in sitting.get_incorrect_questions:
                sitting.remove_incorrect_question(q)
            else:
                sitting.add_incorrect_question(q)

        return self.get(request)

    def get_context_data(self, **kwargs):
        context = super(QuizMarkingDetail, self).get_context_data(**kwargs)
        context['questions'] =\
            context['sitting'].get_questions(with_answers=True)
        return context


class QuizTake(FormView):
    form_class = QuestionForm
    template_name = 'question.html'
    result_template_name = 'result.html'
    single_complete_template_name = 'single_complete.html'

    def dispatch(self, request, *args, **kwargs):
        self.quiz = get_object_or_404(Quiz, url=self.kwargs['quiz_name'])
        try:
            user = User.objects.get(id=self.request.user.id)
        except ObjectDoesNotExist:
            raise PermissionDenied('Log in first.')
        try:
            current_exam = user.sitting_set.last()
        except ObjectDoesNotExist:
            current_exam = None

        if current_exam is not None:
            if current_exam.quiz != self.quiz:
                if current_exam.complete == False:
                    return redirect('quiz_index')

        if self.quiz.draft and not request.user.has_perm('quiz.change_quiz'):
            raise PermissionDenied

        try:
            self.logged_in_user = self.request.user.is_authenticated()
        except TypeError:
            self.logged_in_user = self.request.user.is_authenticated

        if self.logged_in_user:
            self.sitting = Sitting.objects.user_sitting(request.user,
                                                        self.quiz)
        else:
            self.sitting = self.anon_load_sitting()

        if self.sitting is False:
            return render(request, self.single_complete_template_name)

        return super(QuizTake, self).dispatch(request, *args, **kwargs)

    def get_form(self, *args, **kwargs):
        if self.logged_in_user:
            self.question = self.sitting.get_first_question()
            self.progress = self.sitting.progress()
        else:
            self.question = self.anon_next_question()
            self.progress = self.anon_sitting_progress()

        if self.question.__class__ is Essay_Question:
            form_class = EssayForm
        else:
            form_class = self.form_class

        return form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(QuizTake, self).get_form_kwargs()
        # kwargs['use_required_attribute'] = False
        return dict(kwargs, question=self.question)

    def form_valid(self, form):
        if self.logged_in_user:
            self.form_valid_user(form)
            if self.sitting.get_first_question() is False:
                return self.final_result_user()

            user_results = Results.objects.filter(user_id=self.request.user.id)
            individual_result = user_results.first()
            
            def get_equivalent(raw_score,course_equivalent):
                equivalent = ((individual_result.GE_exam + (2*raw_score)) / 40) * course_equivalent
                return equivalent

            CE_equivalent = 0.364309534
            ECE_equivalent = 0.391067378
            EE_equivalent = 0.344729849
            ME_equivalent = 0.382578337

            if individual_result.GE_exam_done and individual_result.CE_exam_done and \
            individual_result.ECE_exam_done and \
            individual_result.EE_exam_done and individual_result.ME_exam_done:
                CE_score = get_equivalent(raw_score=individual_result.CE_exam,course_equivalent=CE_equivalent)
                ECE_score = get_equivalent(raw_score=individual_result.ECE_exam,course_equivalent=ECE_equivalent)
                EE_score = get_equivalent(raw_score=individual_result.EE_exam,course_equivalent=EE_equivalent)
                ME_score = get_equivalent(raw_score=individual_result.ME_exam,course_equivalent=ME_equivalent)
                user_results.update(CE_exam=CE_score, ECE_exam=ECE_score, EE_exam=EE_score, ME_exam=ME_score)
                
            #Sitting.objects.filter(user__username='admin', quiz__title='General Engineering').first().current_score

        else:
            self.form_valid_anon(form)
            if not self.request.session[self.quiz.anon_q_list()]:
                return self.final_result_anon()

        self.request.POST = {}

        return super(QuizTake, self).get(self, self.request)

    def get_context_data(self, **kwargs):
        context = super(QuizTake, self).get_context_data(**kwargs)
        context['question'] = self.question
        context['quiz'] = self.quiz
        if hasattr(self, 'previous'):
            context['previous'] = self.previous
        if hasattr(self, 'progress'):
            context['progress'] = self.progress
        return context

    def form_valid_user(self, form):
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct is True:
            self.sitting.add_to_score(1)
            progress.update_score(self.question, 1, 1)
        else:
            self.sitting.add_incorrect_question(self.question)
            progress.update_score(self.question, 0, 1)

        if self.quiz.answers_at_end is not True:
            self.previous = {'previous_answer': guess,
                             'previous_outcome': is_correct,
                             'previous_question': self.question,
                             'answers': self.question.get_answers(),
                             'question_type': {self.question
                                               .__class__.__name__: True}}
        else:
            self.previous = {}

        self.sitting.add_user_answer(self.question, guess)
        self.sitting.remove_first_question()

    def final_result_user(self):
        results = {
            'quiz': self.quiz,
            'score': self.sitting.get_current_score,
            'max_score': self.sitting.get_max_score,
            'percent': self.sitting.get_percent_correct,
            'sitting': self.sitting,
            'previous': self.previous,
        }

        self.sitting.mark_quiz_complete()

        user_results = Results.objects.filter(user_id=self.request.user.id)
        individual_result = user_results.first()
        if self.quiz == Quiz.objects.get(title='General Engineering'):
            user_results.update(GE_exam=self.sitting.get_current_score, GE_exam_done=True)
        if self.quiz == Quiz.objects.get(title='Civil Engineering'):
            user_results.update(CE_exam=self.sitting.get_current_score, CE_exam_done=True)
        if self.quiz == Quiz.objects.get(title='Electronics Engineering'):
            user_results.update(ECE_exam=self.sitting.get_current_score, ECE_exam_done=True)
        if self.quiz == Quiz.objects.get(title='Electrical Engineering'):
            user_results.update(EE_exam=self.sitting.get_current_score, EE_exam_done=True)
        if self.quiz == Quiz.objects.get(title='Mechanical Engineering'):
            user_results.update(ME_exam=self.sitting.get_current_score, ME_exam_done=True)

        user_results = Results.objects.filter(user_id=self.request.user.id)
        individual_result = user_results.first()
        
        def get_equivalent(raw_score,course_equivalent):
            equivalent = ((individual_result.GE_exam + (2*raw_score)) / 40) * course_equivalent
            return equivalent

        def update_status(user):
            id = User.objects.filter(pk=user.id).values_list('id',flat=True).first()
            obj = User.objects.get(pk=id)
            obj.exam_done = True
            obj.save()

        CE_equivalent = 0.364309534
        ECE_equivalent = 0.391067378
        EE_equivalent = 0.344729849
        ME_equivalent = 0.382578337

        if individual_result.GE_exam_done and individual_result.CE_exam_done and \
        individual_result.ECE_exam_done and \
        individual_result.EE_exam_done and individual_result.ME_exam_done:
            CE_score = get_equivalent(raw_score=individual_result.CE_exam,course_equivalent=CE_equivalent)
            ECE_score = get_equivalent(raw_score=individual_result.ECE_exam,course_equivalent=ECE_equivalent)
            EE_score = get_equivalent(raw_score=individual_result.EE_exam,course_equivalent=EE_equivalent)
            ME_score = get_equivalent(raw_score=individual_result.ME_exam,course_equivalent=ME_equivalent)
            user_results.update(CE_exam=CE_score, ECE_exam=ECE_score, EE_exam=EE_score, ME_exam=ME_score)
            update_status(self.request.user)
        #Sitting.objects.filter(user__username='admin', quiz__title='General Engineering').first().current_score
        if self.quiz.answers_at_end:
            results['questions'] =\
                self.sitting.get_questions(with_answers=True)
            results['incorrect_questions'] =\
                self.sitting.get_incorrect_questions

        if self.quiz.exam_paper is False:
            self.sitting.delete()

        return render(self.request, self.result_template_name, results)

    def anon_load_sitting(self):
        if self.quiz.single_attempt is True:
            return False
        
        # user_results = Results.objects.filter(user_id=self.request.user.id)
        # individual_result = user_results.first()
        
        # def get_equivalent(raw_score,course_equivalent):
        #     equivalent = ((individual_result.GE_exam + (2*raw_score)) / 40) * course_equivalent
        #     return equivalent

        # CE_equivalent = 0.364309534
        # ECE_equivalent = 0.391067378
        # EE_equivalent = 0.344729849
        # ME_equivalent = 0.382578337

        # if individual_result.GE_exam_done and individual_result.CE_exam_done and \
        # individual_result.ECE_exam_done and \
        # individual_result.EE_exam_done and individual_result.ME_exam_done:
        #     print('noice 1')
        #     User.objects.filter(id=self.request.user.id).update(exam_done=True)
        #     CE_score = get_equivalent(raw_score=individual_result.CE_exam,course_equivalent=CE_equivalent)
        #     ECE_score = get_equivalent(raw_score=individual_result.ECE_exam,course_equivalent=ECE_equivalent)
        #     EE_score = get_equivalent(raw_score=individual_result.EE_exam,course_equivalent=EE_equivalent)
        #     ME_score = get_equivalent(raw_score=individual_result.ME_exam,course_equivalent=ME_equivalent)
        #     user_results.update(CE_exam=CE_score, ECE_exam=ECE_score, EE_exam=EE_score, ME_exam=ME_score)
        #     print('noice 2')
        ##Sitting.objects.filter(user__username='admin', quiz__title='General Engineering').first().current_score

        if self.quiz.anon_q_list() in self.request.session:
            return self.request.session[self.quiz.anon_q_list()]
        else:
            return self.new_anon_quiz_session()

    def new_anon_quiz_session(self):
        """
        Sets the session variables when starting a quiz for the first time
        as a non signed-in user
        """
        self.request.session.set_expiry(259200)  # expires after 3 days
        questions = self.quiz.get_questions()
        question_list = [question.id for question in questions]

        if self.quiz.random_order is True:
            random.shuffle(question_list)

        if self.quiz.max_questions and (self.quiz.max_questions
                                        < len(question_list)):
            question_list = question_list[:self.quiz.max_questions]

        # session score for anon users
        self.request.session[self.quiz.anon_score_id()] = 0

        # session list of questions
        self.request.session[self.quiz.anon_q_list()] = question_list

        # session list of question order and incorrect questions
        self.request.session[self.quiz.anon_q_data()] = dict(
            incorrect_questions=[],
            order=question_list,
        )

        return self.request.session[self.quiz.anon_q_list()]

    def anon_next_question(self):
        next_question_id = self.request.session[self.quiz.anon_q_list()][0]
        return Question.objects.get_subclass(id=next_question_id)

    def anon_sitting_progress(self):
        total = len(self.request.session[self.quiz.anon_q_data()]['order'])
        answered = total - len(self.request.session[self.quiz.anon_q_list()])
        return (answered, total)

    def form_valid_anon(self, form):
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct:
            self.request.session[self.quiz.anon_score_id()] += 1
            anon_session_score(self.request.session, 1, 1)
        else:
            anon_session_score(self.request.session, 0, 1)
            self.request\
                .session[self.quiz.anon_q_data()]['incorrect_questions']\
                .append(self.question.id)

        self.previous = {}
        if self.quiz.answers_at_end is not True:
            self.previous = {'previous_answer': guess,
                             'previous_outcome': is_correct,
                             'previous_question': self.question,
                             'answers': self.question.get_answers(),
                             'question_type': {self.question
                                               .__class__.__name__: True}}

        self.request.session[self.quiz.anon_q_list()] =\
            self.request.session[self.quiz.anon_q_list()][1:]

    def final_result_anon(self):
        score = self.request.session[self.quiz.anon_score_id()]
        q_order = self.request.session[self.quiz.anon_q_data()]['order']
        max_score = len(q_order)
        percent = int(round((float(score) / max_score) * 100))
        session, session_possible = anon_session_score(self.request.session)
        if score == 0:
            score = "0"

        results = {
            'score': score,
            'max_score': max_score,
            'percent': percent,
            'session': session,
            'possible': session_possible
        }

        del self.request.session[self.quiz.anon_q_list()]

        if self.quiz.answers_at_end:
            results['questions'] = sorted(
                self.quiz.question_set.filter(id__in=q_order)
                                      .select_subclasses(),
                key=lambda q: q_order.index(q.id))

            results['incorrect_questions'] = (
                self.request
                    .session[self.quiz.anon_q_data()]['incorrect_questions'])

        else:
            results['previous'] = self.previous

        del self.request.session[self.quiz.anon_q_data()]

        return render(self.request, 'result.html', results)


def anon_session_score(session, to_add=0, possible=0):
    """
    Returns the session score for non-signed in users.
    If number passed in then add this to the running total and
    return session score.

    examples:
        anon_session_score(1, 1) will add 1 out of a possible 1
        anon_session_score(0, 2) will add 0 out of a possible 2
        x, y = anon_session_score() will return the session score
                                    without modification

    Left this as an individual function for unit testing
    """
    if "session_score" not in session:
        session["session_score"], session["session_score_possible"] = 0, 0

    if possible > 0:
        session["session_score"] += to_add
        session["session_score_possible"] += possible

    return session["session_score"], session["session_score_possible"]
