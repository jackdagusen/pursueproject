# Generated by Django 3.2.3 on 2022-01-14 14:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0002_auto_20211119_2034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='figure',
            field=models.ImageField(blank=True, null=True, upload_to='media', verbose_name='Figure'),
        ),
    ]
